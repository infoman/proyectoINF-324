@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">You are logged in!</div>

                <div class="panel-body contenedor_botones">
                    <!--Dirige al panel de la secretaria-->
                    <a href="{{ url('/cliente') }}" class="botonesPrincipales"><img src="img/user.jpg" alt="" /><br>Secretaria</a>
                    <!--Dirige al panel del locutor-->
                    <a href="{{ url('/alquiler') }}" class="botonesPrincipales"><img src="img/user.jpg" alt="" /><br>Locutor</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
