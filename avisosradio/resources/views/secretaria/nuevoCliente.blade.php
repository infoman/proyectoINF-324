@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Secretaria</div>

                <div class="panel-body contenedor" align="center" style="padding: 0px 0px 0px 0px;">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="list-group">
                        <a href="{{ url('/cliente') }}" class="list-group-item">Nuevo aviso</a>
                        <a href="{{ url('/clientenuevo') }}" class="list-group-item active">Nuevo cliente</a>
                        <a href="{{ url('/aviso') }}" class="list-group-item">Todos los avisos</a>
                      </div>
                    </div>
                    <div class="col-md-8">
                      @if (Session::has('message'))
                         <div class="alert alert-success">{{ Session::get('message') }}</div>
                      @endif
                      @if($errors->has())
                          <div class='alert alert-danger'>
                              @foreach ($errors->all('<p>:message</p>') as $message)
                                  {!! $message !!}
                              @endforeach
                          </div>
                      @endif
                      {!! Form::open(array('route' => 'cliente.store', 'method' => 'POST')) !!}
                        <div class="form-group">
                          {!! Form::label('Nombres:') !!}
                          {!! Form::text('nombres', null, ['class'=>'form-control', 'placeholder'=>'Nombre de cliente']) !!}
                        </div>
                        <div class="form-group">
                          {!! Form::label('Apellidos:') !!}
                          {!! Form::text('apellidos', null, ['class'=>'form-control', 'placeholder'=>'Apellidos de cliente']) !!}
                        </div>
                        <div class="form-group">
                          {!! Form::label('Ci:') !!}
                          {!! Form::text('ci', null, ['class'=>'form-control', 'placeholder'=>'Ci de cliente']) !!}
                        </div>
                        <div class="form-group">
                          {!! Form::label('Procedencia:') !!}
                          {!! Form::text('procedencia', null, ['class'=>'form-control', 'placeholder'=>'Procedencia ci']) !!}
                        </div>
                        <div class="form-group">
                          {!! Form::label('Nit:') !!}
                          {!! Form::text('nit', null, ['class'=>'form-control', 'placeholder'=>'Nit cliente']) !!}
                        </div>
                        <div class="form-group">
                          {!! Form::submit('Aceptar', ['class'=>'btn btn-primary']) !!}
                        </div>
                      {!! Form::close() !!}
                    </div>
                  </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
