@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Secretaria</div>

                <div class="panel-body contenedor" align="center" style="padding: 0px 0px 0px 0px;">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="list-group">
                        <a href="{{ url('/cliente') }}" class="list-group-item active">Nuevo aviso</a>
                        <a href="{{ url('/clientenuevo') }}" class="list-group-item">Nuevo cliente</a>
                        <a href="{{ url('/aviso') }}" class="list-group-item">Todos los avisos</a>
                      </div>
                    </div>
                    <div class="col-md-8">
                      @if (Session::has('message'))
                         <div class="alert alert-success">{{ Session::get('message') }}</div>
                      @endif
                      @if($errors->has())
                          <div class='alert alert-danger'>
                              @foreach ($errors->all('<p>:message</p>') as $message)
                                  {!! $message !!}
                              @endforeach
                          </div>
                      @endif
                      {!! Form::open(array('route' => 'aviso.store', 'method' => 'POST')) !!}
                      <div class="form-group selection">
                        {!! Form::label('Clase:') !!}
                        {{ Form::select('clase', ['Alquiler' => 'Alquiler', 'Oficio solicitado' => 'Oficio solicitado', 'Oficio ofrecido' => 'Oficio ofrecido', 'Venta' => 'Venta', 'Compra' => 'Compra'],null, ['class' => 'form-control']) }}
                      </div>
                      <div class="form-group selection">
                        {!! Form::label('Tipo:') !!}
                        <!--
                          A = 20 palabras 5bs
                          B = 35 palabras 8bs
                          C = 45 palabras 10bs
                        -->
                        {{ Form::select('tipo', ['A' => 'A (5 Bs.)', 'B' => 'B (8 Bs.)', 'C' => 'C (10 Bs.)'], null, ['class' => 'form-control']) }}
                      </div>
                      <div class="form-group selection">
                        {!! Form::label('Cliente: ') !!}
                        <select class="form-control" name="id_cliente">
                          @foreach($clientes as $cliente)
                          <option value="{{$cliente->id}}">{{$cliente->nombres}} {{$cliente->apellidos}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        {!! Form::label('Descripción: ') !!}
                        {{ Form::textarea('descripcion', null, ['placeholder'=>'Insertar descripción del aviso', 'class' => 'form-control']) }}
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                      </div>
                      {!! Form::close() !!}
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
