@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Secretaria</div>

                <div class="panel-body contenedor" align="center" style="padding: 0px 0px 0px 0px;">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="list-group">
                        <a href="{{ url('/cliente') }}" class="list-group-item">Nuevo aviso</a>
                        <a href="{{ url('/clientenuevo') }}" class="list-group-item">Nuevo cliente</a>
                        <a href="{{ url('/aviso') }}" class="list-group-item active">Todos los avisos</a>
                      </div>
                    </div>
                    <div class="col-md-8">
                      @if (Session::has('message'))
                         <div class="alert alert-success">{{ Session::get('message') }}</div>
                      @endif
                      <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <th>Clase</th>
                                <th>Tipo</th>
                                <th>Descripción</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($avisos as $aviso)
                              <tr>
                                <td>{{$aviso->clase}}</td>
                                <td>{{$aviso->tipo}}</td>
                                <td>{{$aviso->descripcion}}</td>
                                <td>
                                    {{link_to_route('aviso.edit', $title = 'Editar', $parameters = $aviso->id, $attributes = array('class' => 'btn btn-success'))}}
                                </td>
                                <td>
                                    {{Form::open(['route'=>['aviso.destroy', $aviso->id], 'method'=>'DELETE'])}}
                                      {{Form::submit('Eliminar', ['class'=>'btn btn-danger'])}}
                                    {{Form::close()}}
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
