@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Locutor</div>

                <div class="panel-body contenedor" align="center" style="padding: 0px 0px 0px 0px;">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="list-group">
                        <a href="{{ url('/alquiler') }}" class="list-group-item {{$estadoMenu1}}">Alquiler</a>
                        <a href="{{ url('/solicitado') }}" class="list-group-item {{$estadoMenu2}}">Oficio solicitado</a>
                        <a href="{{ url('/ofrecido') }}" class="list-group-item {{$estadoMenu3}}">Oficio ofrecido</span></a>
                        <a href="{{ url('/venta') }}" class="list-group-item {{$estadoMenu4}}">Venta</a>
                        <a href="{{ url('/compra') }}" class="list-group-item {{$estadoMenu5}}">Compra</a>
                      </div>
                    </div>
                    <div class="col-md-8">
                      @if (Session::has('message'))
                         <div class="alert alert-success">{{ Session::get('message') }}</div>
                      @endif
                      <div class="list-group">
                        @foreach($avisosNuevos as $avisosNuevo)
                        <a href="#" class="list-group-item">
                          <h4 class="list-group-item-heading">Tipo: {{$avisosNuevo->tipo}}</h4>
                          <p class="list-group-item-text">{{$avisosNuevo->descripcion}}
                            {{link_to_route('editaAviso.edit', $title = 'Leer', $parameters = $avisosNuevo->id, $attributes = array('class' => 'btn btn-success'))}}
                          </p>
                        </a>
                        @endforeach
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
