<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('avisos', function (Blueprint $table) {
          $table->increments('id')->unique();
          $table->integer('id_cliente')->unsigned();
          $table->string('clase');
          $table->string('tipo');
          $table->text("descripcion");
          $table->integer('estado')->default(0); //0 no leido 1 leido
          $table->rememberToken();
          $table->timestamps();
      });
      Schema::table('avisos', function($table) {
        $table->foreign('id_cliente')->references('id')->on('clientes')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
