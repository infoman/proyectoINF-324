<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('facturas', function (Blueprint $table) {
          //$table->engine = 'InnoDB'
          $table->increments('id')->unique();
          $table->integer('nro_factura');
          $table->string('detalle');
          $table->integer('costo_total');
          $table->rememberToken();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
