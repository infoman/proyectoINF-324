<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $fillable = ['nombres', 'apellidos', 'ci', 'procedencia', 'nit'];
    protected $guarded = ['id'];
}
