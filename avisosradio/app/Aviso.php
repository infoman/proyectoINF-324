<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aviso extends Model
{
  protected $table = 'avisos';
  protected $fillable = ['id_cliente', 'clase', 'tipo', 'descripcion'];
  protected $guarded = ['id'];
}
