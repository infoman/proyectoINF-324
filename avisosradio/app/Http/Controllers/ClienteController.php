<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Cliente;

use App\Http\Requests;
use App\Http\Requests\ClienteForm;

class ClienteController extends Controller
{
  public function index()
  {
    $clientes = Cliente::all();
    return view('secretaria.secretaria', compact('clientes'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(ClienteForm $request)
  {
    $cliente = $request->all();
    Cliente::create($cliente);
    return redirect('clientenuevo')->with('message', 'Cliente guardado correctamente');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    //
  }
}
