<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Aviso;
use DB;

use Session;
use Redirect;

use App\Http\Requests;
use App\Http\Requests\AvisoForm;

class AvisoController extends Controller
{
  public function index()
  {
    $avisos = Aviso::all();
    return view('secretaria.listaavisos');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(AvisoForm $request)
  {
    $aviso = $request->all();
    Aviso::create($aviso);
    return redirect('cliente')->with('message', 'Aviso guardado correctamente');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $aviso = Aviso::find($id);
    return view('secretaria.editaAviso', ['aviso'=>$aviso]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id, AvisoForm $request)
  {
    $aviso = Aviso::find($id);
    $aviso->fill($request->all());
    $aviso->save();
    Session::flash('message', 'Aviso actualizado correctamente');
    return Redirect::to('/aviso');
    //return redirect('aviso')->with('message', 'Actualizado correctamente');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    Aviso::destroy($id);
    Session::flash('message', 'Aviso eliminado correctamente');
    return Redirect::to('/aviso');
  }
  public function avisoSinLeer1(){//Alquiler
    $avisosNuevos = DB::table('avisos')->where([
                ['estado','0'],
                ['clase','Alquiler']
            ])->orderBy('id', 'desc')->get();
            $estadoMenu1 = "active";
            $estadoMenu2 = "";
            $estadoMenu3 = "";
            $estadoMenu4 = "";
            $estadoMenu5 = "";
    return view('locutor.locutor', compact('avisosNuevos', 'estadoMenu1', 'estadoMenu2', 'estadoMenu3', 'estadoMenu4', 'estadoMenu5'));
  }
  public function avisoSinLeer2(){//Oficio solicitado
    $avisosNuevos = DB::table('avisos')->where([
                ['estado','0'],
                ['clase','Oficio solicitado']
            ])->orderBy('id', 'desc')->get();
            $estadoMenu1 = "";
            $estadoMenu2 = "active";
            $estadoMenu3 = "";
            $estadoMenu4 = "";
            $estadoMenu5 = "";
        return view('locutor.locutor', compact('avisosNuevos', 'estadoMenu1', 'estadoMenu2', 'estadoMenu3', 'estadoMenu4', 'estadoMenu5'));
  }
  public function avisoSinLeer3(){//Oficio ofrecido
    $avisosNuevos = DB::table('avisos')->where([
                ['estado','0'],
                ['clase','Oficio ofrecido']
            ])->orderBy('id', 'desc')->get();
            $estadoMenu1 = "";
            $estadoMenu2 = "";
            $estadoMenu3 = "active";
            $estadoMenu4 = "";
            $estadoMenu5 = "";
        return view('locutor.locutor', compact('avisosNuevos', 'estadoMenu1', 'estadoMenu2', 'estadoMenu3', 'estadoMenu4', 'estadoMenu5'));
  }
  public function avisoSinLeer4(){//Venta
    $avisosNuevos = DB::table('avisos')->where([
                ['estado','0'],
                ['clase','Venta']
            ])->orderBy('id', 'desc')->get();
            $estadoMenu1 = "";
            $estadoMenu2 = "";
            $estadoMenu3 = "";
            $estadoMenu4 = "active";
            $estadoMenu5 = "";
        return view('locutor.locutor', compact('avisosNuevos', 'estadoMenu1', 'estadoMenu2', 'estadoMenu3', 'estadoMenu4', 'estadoMenu5'));
  }
  public function avisoSinLeer5(){//Compra
    $avisosNuevos = DB::table('avisos')->where([
                ['estado','0'],
                ['clase','Compra']
            ])->orderBy('id', 'desc')->get();
            $estadoMenu1 = "";
            $estadoMenu2 = "";
            $estadoMenu3 = "";
            $estadoMenu4 = "";
            $estadoMenu5 = "active";
        return view('locutor.locutor', compact('avisosNuevos', 'estadoMenu1', 'estadoMenu2', 'estadoMenu3', 'estadoMenu4', 'estadoMenu5'));
  }
  public function actualizaAviso(){
    return "aviso actualizado";
  }
}
