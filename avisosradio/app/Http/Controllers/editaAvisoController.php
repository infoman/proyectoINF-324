<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class editaAvisoController extends Controller
{
  public function index()
  {
    //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(ClienteForm $request)
  {

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $aviso = DB::table('avisos')
            ->where('id', $id)->get();
    $dir = "";
    if($aviso[0]->clase == 'Alquiler'){
      $dir = 'alquiler';
    }
    else if($aviso[0]->clase == 'Oficio solicitado'){
      $dir = 'solicitado';
    }
    else if($aviso[0]->clase == 'Oficio ofrecido'){
      $dir = 'ofrecido';
    }
    else if($aviso[0]->clase == 'Venta'){
      $dir = 'venta';
    }
    else if($aviso[0]->clase == 'Compra'){
      $dir = 'compra';
    }
    DB::table('avisos')
            ->where('id', $id)
            ->update(array('estado' => 1));
    return redirect($dir)->with('message', 'Mensaje leido');
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    //
  }
}
