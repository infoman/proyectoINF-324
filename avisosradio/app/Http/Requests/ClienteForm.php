<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClienteForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres' => 'required',
            'apellidos' => 'required',
            'ci' => 'required',
            'procedencia' => 'required',
            'nit' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'nombres.required' => 'El campo nombres es requerido',
            'apellidos.required' => 'El campo apellidos es requerido',
            'ci.required' => 'El campo CI es requerido',
            'procedencia.required' => 'El campo procedencia es requerido',
            'nit.required' => 'El campo Nit es requerido'
        ];
    }
}
