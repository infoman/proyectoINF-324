<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Cliente;
use App\Aviso;

class AvisosComposer{

  public function compose(View $view)
  {
    $clientes = Cliente::all();
    $view->with('clientes', $clientes);
    $avisos = Aviso::all();
    $view->with('avisos', $avisos);
  }
}
