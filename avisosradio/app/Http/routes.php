<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('welcome'); //Usuarios invitados
});

Route::auth(); //Rutas de autenticación

Route::get('/home', 'HomeController@index'); //Usuario ya logueados

/*Route::get('/secretaria', function(){
  return view('secretaria.secretaria');
});*/

Route::get('/locutor', function(){
  return view('locutor.locutor');
});

Route::get('/clientenuevo', function(){
  return view('secretaria.nuevoCliente');
});

/*Route::get('/listaavisos', function(){
  return view('secretaria.listaAvisos');
});*/
//Direcciones CRUD
Route::resource('cliente', 'ClienteController');
Route::resource('aviso', 'AvisoController');
Route::resource('editaAviso', 'editaAvisoController');

Route::resource('alquiler', 'AvisoController@avisoSinLeer1');
Route::resource('solicitado', 'AvisoController@avisoSinLeer2');
Route::resource('ofrecido', 'AvisoController@avisoSinLeer3');
Route::resource('venta', 'AvisoController@avisoSinLeer4');
Route::resource('compra', 'AvisoController@avisoSinLeer5');

//******MIS RUTAS
Route::get('/prueba', function () {
    return view('prueba');
});
